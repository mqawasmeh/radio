import { Component, ResolvedReflectiveProvider } from '@angular/core';
import { LoadingController, NavController, AlertController, Platform ,ToastController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { environment } from 'src/environments/environment';
import { Network } from '@ionic-native/network/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  name = "play-circle";
  play = 0;
  count = 1;
  audio = new Audio();
  soundRadio = [];
  name_sound = "volume-high";
  disconnectSubscription;
  connectSubscription;

  noEnternet() {
    this.dialogs.alert('ناسف انت غير متصل بالانترنت');
  }
  constructor(private dialogs:Dialogs,private platform:Platform,public loadingController:LoadingController,private network: Network,public localNotifications: LocalNotifications ) {
    this.platform.ready().then(async () => {

      
      if(this.network.type == 'none'){
        this.pauseAudio();
        this.name = "play-circle";
      }
      else{
        this.name = "play-circle";
        this.pauseAudio();
        this.connectdatabase();
      }
      this.network.onConnect().subscribe(() => {
        this.loadconnect();
        this.connectdatabase();
        this.name = "play-circle";
        this.pauseAudio();
      });
      this.network.onDisconnect().subscribe(()=>{
        this.pauseAudio();
        this.name = "play-circle";
      });
    });
  }
  connectdatabase(){
    this.audio.volume = 0.5;
    firebase.initializeApp( environment.firebase );
    let db = firebase.firestore();
    db.collection('RADIO').get().then((ref) => {
      this.soundRadio = ref.docs.map(doc => doc.data());
    });
  }

  addAudio(id) {
    if(this.network.type == 'none'){
      this.noEnternet();
    }
    else{
      this.play = id;
      this.presentLoading();
      this.name = "play-circle";
      this.playAudio();
    }
  }
  
  public playAudio():void {
    if(this.network.type != 'none'){
      if( this.name == "pause") {
        this.pauseAudio();
      }

      else {
        if(this.play>=0&&this.play<this.soundRadio.length) {

          this.audio.src = this.soundRadio[this.play].Audio;
          this.audio.load();
          this.audio.play();   
          if(this.soundRadio[this.play].FM === "103.2FM" || this.soundRadio[this.play].name === "راديو يبوس") {
            this.loadPlay(5000);
          }
          else if(this.soundRadio[this.play].FM === "94.8FM" ){
            this.loadPlay(6000);
          }
          else if(this.soundRadio[this.play].name === "راديو اجيال" || this.soundRadio[this.play].name === "إذاعة الهدى") {
            this.loadPlay(1500);
          }

          else {
            this.loadPlay(300);
          }
        }
      }
    }
    else {
      this.noEnternet();
    }
  }
  public pauseAudio() {
    if(this.network.type == 'none') {
      this.noEnternet();
    }
    this.audio.pause();
    this.name = "play-circle";
  }

  public nextAudio() {
    if(this.network.type != 'none') {
      if(this.play != this.soundRadio.length - 1 ) {

        this.play++;
        this.name = "play-circle";
        this.playAudio();

      }
    }
    else {
      this.noEnternet();
    }
  }
  public previousAudio() {
    if(this.network.type != 'none') {
      if(this.play != 0 ) {

        this.play--;
        this.name = "play-circle";
        this.playAudio();

      }
    }
    else {
      this.noEnternet();
    }
  }
  
  public valumAudio(value) {

    this.audio.volume = (value/100.0) ;
    
    if(value == 0){
      this.name_sound = "volume-mute";
    }
    else if (value < 40){
      this.name_sound = "volume-low";
    }
    else{
      this.name_sound = "volume-high";

    }
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'الرجاء الانتظار ',
      duration: 300
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
  }
  
  async loadPlay(sleep) {
    const loading = await this.loadingController.create({
      message: 'الرجاء الانتظار ',
      duration: sleep
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
   
    this.name = "pause";
  }

  async loadconnect() {
    const loading = await this.loadingController.create({
      message: 'الرجاء الانتظار ',
      duration: 1000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
   
  }
  async loadconnect1() {
    const loading = await this.loadingController.create({
      message: 'الرجاء الانتظار ',
      duration: 1000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    if(this.network.type == 'none'){
     this.name = "play-circle";
     this.dialogs.alert('ناسف انت غير متصل بالانترنت');
    }
    else{
      this.connectdatabase();
    }
   
  }
  Refresh(event){
      setTimeout(() => {
       event.target.complete();
      }, 1000);
      if(this.network.type == 'none'){
        this.noEnternet();
      }
      else {
        this.connectdatabase();
      }
  }


  // items = Array.from(document.getElementById('search').childNodes);
  search(event) {
    console.log(event);
    var count = 0;
    for(var i=0;i<this.soundRadio.length;i++){
      if(this.soundRadio[i].name.search(event)==-1){
        document.getElementById("h_"+i).style.display ='none';
      }
      else{
        document.getElementById("h_"+i).style.display ='block';
        count++;
      }
    }
    if(count == 0){
      for(var i=0;i<this.soundRadio.length;i++){
        document.getElementById("h_"+i).style.display ='block';
      } 
    }
  }
  onCancel($event){
     for(var i=0;i<this.soundRadio.length;i++){
        document.getElementById("h_"+i).style.display ='block';
      } 
  }

  ionViewDidEnter(){
    this.platform.backButton.subscribe(()=>{
        navigator['app'].pauseApp();
    });
  }
}
