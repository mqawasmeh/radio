// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCP9_7FgiKTeTvwf04kE_sLyqqIre_s4t8",
    authDomain: "bradio-5dc17.firebaseapp.com",
    databaseURL: "https://bradio-5dc17.firebaseio.com",
    projectId: "bradio-5dc17",
    storageBucket: "bradio-5dc17.appspot.com",
    messagingSenderId: "960257540006",
    appId: "1:960257540006:web:7598c652dfab35142c5967",
    measurementId: "G-FRF0MLYCLL"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
